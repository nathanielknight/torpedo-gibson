# Project Codename "Torpedo Gibson"

A text-based browser game of spending time and seeing consequences.

## Setup

To develop, run `lein figwheel` at the top of the project directory
and browse [localhost:3449](http://localhost:3449/). You'll get a
live-reloading version of the app.

Clojurescript code under `scr/torpedo-gibson` can be edited (but many
things are `defonce`'d mind, so you'll have to update those by
hand). You'll get a REPL in your terminal which can be used to examine
and experiment with the app's state.

To clean all compiled files: `lein clean`

To create a production build run: `lein do clean, cljsbuild once min`
And open your browser in `resources/public/index.html`. You will not
get live reloading, nor a REPL.

## License

Copyright © 2016 Nathaniel Knight

All Right's Reserved (for now)
