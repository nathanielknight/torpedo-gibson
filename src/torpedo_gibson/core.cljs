(ns torpedo-gibson.core
  "Torpedo-gibson browser game."
  (:require [reagent.core :as r]
            [clojure.string :as string]))

(enable-console-print!)


;;;; Game Data

(defonce resource-names
  {
   :time "Time"
   :money "Money"
   :experience "Experience"
   :disease "Disease"
   :regret "Regret"
   :affection "Affection"
   :love "Love"
   :true-love "True Love"
   })

(def payoffs "A map from sets of selected thing-ids to static results"
  {
   #{:time} {:experience 1, :time -1}
   #{:time :experience} {:money 1, :time -1}
   #{:time :money} {:experience 2, :time -1, :money -1}
   #{:time :affection} {:time -1}
   })

(def possible-moves (-> payoffs keys set))

(defonce starting-resources {:time 10})

(defonce initial-state {:resources starting-resources
                        :history {}
                        :selected-resource-ids #{}})

(defn valid-app-state? "Rules to enforce on the app-state" [app-state]
  ;; Resources > 0
  (let [valid   (every? #(<= 0 %) (-> app-state :resources vals))]
    (when (not valid)
      (println app-state))
    valid))

(defonce app-state (r/atom initial-state
                           :validator valid-app-state?))


;;;; Game Logic

(defn can-move? [selected-ids]
  (contains? possible-moves selected-ids))

(defn resources-add [resources updates]
  (reduce (fn [m [k v]] (update m k + v))
          resources
          updates))

(defn resources-normalize "Remove 0 values"
  [resources]
  (->> resources
      (filter (fn [[k v]] (< 0 v)))
      (into {})))

(defn resources-get-random "Random extra resources"
  [resources history selected-ids]
  (reduce
   #(resources-add %1 (%2)) {}
   (list
    ;; experience yields wisdom
    #(when (< 3 (get history :experience)) {:wisdom 1})
    ;; disease robs you of time
    #(when (contains? resources :disease) {:time -1})
    ;; disease may strike anytime
    #(when (= 0 (rand-int 10)) {:disease 1})
    ;; if you pursue only money you'll have regrets
    #(when (and (< (get resources :experience 0) (get resources :money 0))
                (= 0 (rand-int 3))) {:regret 1})
    ;; affection may increase or subside
    #(if  (and (contains? selected-ids :affection)
                (not= 0 (rand-int 3)))
       {:affection 1}
       {:affection -1})
    ;; affection may turn to love
    #(when (and (contains? selected-ids :affection)
                (= 0 (rand-int 3)))
       {:love 1})
    ;; love may be true
    #(when (and (contains? selected-ids :love)
                (= 0) (rand-int 100))
       {:true-love 1})
    )))

(defn updated-appstate
  "The new resources map based on an existing one, spent history,
  and the resources being combined."
  [resources history selected-ids]
  (let [static-resources (get payoffs selected-ids {:time -1})
        random-resources (resources-get-random resources selected-ids history)]
    {:resources (-> resources
                    (resources-add static-resources)
                    (resources-add random-resources)
                    resources-normalize)
     :history (reduce history #(update %1 %2 + 1) selected-ids)
     :selected-resource-ids selected-ids}))

(defn combine!
  "Determine which things have been selected and combine them into a new thing"
  []
  (let [selected-ids (:selected-resource-ids @app-state)
        resources (:resources @app-state)
        history (:history @app-state)]
    (cond
      ;; A legal move is selected
      (can-move? selected-ids)
      (reset! app-state (updated-appstate resources history selected-ids))
      :else
      nil)))


;;;; Views

(defn render-resource [id amt]
  [:li
   (when (not (contains? (get @app-state :selected-resource-ids #{}) id))
     [:button
      {:on-click
       #(let [selected-resource-ids (get @app-state :selected-resource-ids)]
          (swap! app-state update-in [:selected-resource-ids]
                 conj id))}
      "+"])
   (str amt " ")
   (get resource-names id "Unknown Resource!!!")])

(defn render-resources [resources]
  [:ul#resources
   (for [[resource amount] resources :when (> amount 0)]
     ^{:key resource} [render-resource resource amount])])

(defn render-selected-resource-id [id]
  [:li
   [:button
   {:on-click
        #(swap! app-state update-in [:selected-resource-ids]
                disj id)}
    "-"]
   (get resource-names id)])

(defn render-selected-resource-ids [selected-resource-ids]
  [:ul#selected
   (for [id selected-resource-ids]
     ^{:key id} [render-selected-resource-id id])])

(defn proceed-button []
  (let [active? (can-move? (:selected-resource-ids @app-state))]
    [:button#proceed {:on-click (fn [e] (when active? (combine!)))
                      :class (if active? "active" "inactive")}
     "Proceed"]))

(defn reset-button []
  [:span {:on-click (fn [e] (reset! app-state initial-state))
          :style {:background-color :orange}}
   "Reset"])


;;;; Main

(defn app [app-state]
  (if (< 0 (get-in @app-state [:resources :time]))
    [:div
     [:p "Choose resources:"]
     [render-resources (get @app-state :resources)]
     [:div#proceed
      [render-selected-resource-ids (get @app-state :selected-resource-ids)]
      [proceed-button]]
     ]
    [:div
     [:p "You're out of time. This is all you had:"]
      [render-resources (get @app-state :resources)]
      [reset-button]
]))


(defn ^:export run []
  (r/render [app app-state]
            (js/document.getElementById "app")))
(run)
